#include <stdio.h>

void triR(void) {
  int size, repeat;
	scanf("%d %d", &size, &repeat);
	printf("Hello world\n");
     while(0<repeat)
     {
     printf("\n");
     int k;
     for(k=1;k<=size;k++)
     {
         for (int i=1; i<=k;i++)
         {
             printf("%d",k);
         }
         printf("\n");
     }
     int a,b;
     for(a=size-1;a>=1;a--)
     {
         for (b=1; b<=a;b++)
         {
             printf("%d",a);
         }
         printf("\n");
     }
     repeat--;
     }

	printf("Bye world\n");
	return 0;
}

void triL(void) {
  int size, repeat;
	scanf("%d %d", &size, &repeat);
	printf("Hello world\n");

  // ...
  // 이 함수를 완성하시오. (1점)
  // ...

	printf("Bye world\n");
}

void dias(void) {
  int size, repeat;
	scanf("%d %d", &size, &repeat);
	printf("Hello world\n");

  // ...
  // 이 함수를 완성하시오. (1점)
  // ...

	printf("Bye world\n");
}

int main(void)
{
  int n;
  scanf("%d", &n); // 1,2,3 중 하나를 입력받는다
  switch (n)
  {
    case 1: triR(); break;
    case 2: triL(); break;
    case 3: dias(); break;
    default: return -1;
  }
  return 0;
}
